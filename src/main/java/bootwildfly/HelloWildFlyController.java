package bootwildfly;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWildFlyController {


    @RequestMapping("giovanni")
    public String sayHello(){
        return ("Ciao sono Giovanni");
    }
}